---
title: DIY House with AR
description: An approach to visualize DIY House process
author: Wei Yang
keywords: marp,marp-cli,slide,AR
url: https://w-yang-de.gitlab.io/home-diy-with-ar | https://rwth-crmasters-sose22.gitlab.io/shared-runner-group/home-diy-with-ar/
marp: true
image: assets/galok.webp
---

# Collaborative e**X**tended **R**eality for Construction

Many thanks to **_Georgy(Ye Lu)_**, **_David_**, **_Enes_**, **_Rebeca_** and all others, who inpired and encouraged my concept.

![bg left](assets/galok.webp)

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

---
# Inspirations
- [From BIM to extended reality in AEC industry](https://www.academia.edu/43824998/From_BIM_to_extended_reality_in_AEC_industry)
- [Using Augmented Reality (AR) for pre-, during & post- construction](https://www.youtube.com/watch?v=_uW1s6XRqS8&t=2238s)
- [vGIS Case Study: Using AR to improve efficiency during a bridge replacement project](https://www.vgis.io/vgis-ar-augmented-reality-case-study-bridge-project-digital-construction-innovation-hololens/)
- [High-accuracy augmented reality (AR) in construction](https://www.youtube.com/watch?v=3cJUJVPimgc)
- [Microsoft Mesh](https://www.youtube.com/watch?v=Jd2GK0qDtRg)
- [ARKi Demo](https://www.youtube.com/watch?v=K_FeZuFsYK0)
- [Wood Blocksystem](https://www.gablok-deutschland.de/gablok-modulare-konstruktionsmethode-blocksystem)
- [3D Instructions for LEGO](https://buildin3d.com/)
- [Buiding A Minecraft Structure for the real world](https://education.bentley.com/Organized/bentleyHosted-624493)
- [Minecraft Earth in AR at Apple WWDC 2019](https://www.youtube.com/watch?v=GNo38kNy_EU)

---
# AEC Industry Roadmap
![w:640 center](assets/AEC_industry_roadmap.webp) 

---
# Mindmaps

::: fence
@startuml
@startmindmap
+ Construcion Information
++ BIM
+++ Design
+++[#lightblue] Simulation 
+++ Cost
++ Team
+++ Project Manager
++++ Project Management
++++ Team Management
+++ Architect
+++ Civil Engineer
+++ Mechanical Engineer
+++ Worker
+++ Other Support Member
-- Material
--- Blocks
--- Concrete
--- Other Material
-- Site
---[#lightblue] Location
--- Size
--- Geological Condition
--- Weather 
-- Tools
--- Machinery
---- Crane
---- Excavator
----[#orange] Robot
---- Other Machines
--- Scaffold
--- Hand Tools
@endmindmap

@enduml
:::

---
# Collaborative e**X**tended **R**eality for Construction

- Too many papers for construction work
- Understanding vary with backgrounds 
- With AR, instructions could be easier to follow
- Increase precision for **less-experienced** starters
- A path to integration with robot and automation
- Prototype in small-scaled Home DIY with blocks

---
# Compare with Kuka|crc

## KUKA|crc Cloud Remote Control


- Grasshoper may not have enough performances for simulation.
- Too complex with scripts

## XR based Control

- Shared by human and robot
- Progressive Development to Automation
- Robot need to have ability to follow virtual rag/target and move autonomously
- A bridge with Digital Twins

---
# Feedbacks from Insdustry
Remarks from **_Georgy (Ye Lu)_**'s friend in construcion prefession:

- Talets beats AR Glasses for noting and drawing
- AR has more potential for planner/architects and clients, may not for craft worker
- **TOO MUCH** Digital Drawing works already
- Mostly Refurbishments and extensions projects
- Short time restraint -> dense schedule -> one delay leads to chain reaction
- Functionality, costs and (time-)efficiency mostly beat aesthetics and “the last 2 %” of precision
- Good relations to all help

---
# Responce to the feedbacks:
- Tablet Support / Draw in the Air
- Simply operations - Voice Input / Gesture with Glove
- Provide informaiton for current building
- Collaboration with different backgrounds
- Social Activity Support
- Innovations are ususally under-estimated

---
# Blocks for integration
- [Rhino/Grasshopper](https://www.rhino3d.com/) for Design
- [iTwin](https://www.itwinjs.org/)/[iModel](https://www.bentley.com/en/i-models/what-is-i-model/about-i-models) for model exchange
- [Speckle Connectors](https://speckle.systems/) for Extract and exchange data
- [Unity3D](https://unity.com/)/[Godot](https://godotengine.org/)/[BevyEngine](https://bevyengine.org/) for cross-platform AR
- [ARKi](https://www.darfdesign.com/arki.html) / [JigSpace](https://www.jig.space/) for AR View
- [Cloud Anchor](https://developers.google.com/ar/develop/cloud-anchors) / [Spacial Anchor](https://azure.microsoft.com/de-de/services/spatial-anchors/)/ [GeoLocation Anchor](https://developer.apple.com/documentation/arkit/content_anchors/tracking_geographic_locations_in_ar)

---
# Open Soure Projects/Demo
- [x] [Interactive Real-Time Experiences with Unity, Revit, Rhino and Speckle](https://github.com/specklesystems/speckle-unity) : Only works in Editor
- [x] [iTwin Unity samples](https://github.com/iTwin/imodel-unity-example)
- [ ] [Azure Spatial Anchor](https://github.com/Azure/azure-spatial-anchors-samples)
- [ ] [Google Cloud Anchor](https://developers.google.com/ar/develop/ios/cloud-anchors/quickstart)
- [ ] [Tracking Geographic Locations in AR](https://developer.apple.com/documentation/arkit/content_anchors/tracking_geographic_locations_in_ar)
 
---
# My Approach
- [x] Try Speckle Demo for Unity/Rhino - Only works in Editor
- [x] Try iTwin Unity Sample
- [ ] Add AR feature
- [ ] Add GeoLocation
- [ ] Comparw with ARKi/Jigspace
- [ ] Some research on robot control with AR

---
# What I may learn
- [ ] Collaboration with diferent backgrounds/culture/language
- [ ] AEC real problems / practices 
- [ ] Workflow of iTwinjs - Digital Twins Platform in nodejs
- [ ] Develop AR on iOS with Unity3D/RealityKit/ARKit
- [ ] Fix package compatible issues in Unity
- [ ] 3D presentation with ARKi/JigSpace

---
# Thanks for your attention.
- Any Thought or Questions?
